import './RelatedCard.css';
import { Link } from 'react-router-dom';
import React from 'react';
import PropTypes from 'prop-types';

const RelatedCard = (props) => {
    // Generate a random number between 1 and 1000
    const random = Math.floor(Math.random() * 50) + 1;
    return (
        <div className="related__product__card__container">
            <div className="related__product__card__inner">
                <div className="related__product__image">
                    <img src={`https://picsum.photos/id/${random}/500/620`} alt="item" className="product__img"/>
                </div>
                <div className="related__product__card__detail">
                    <div className="related__product__name">
                        <Link to={`/product/${props.item.slug}`}>
                            {props.item.name}
                        </Link>

                    </div>
                    <div className="related__product__description">
                        <span>{props.item.description}</span>
                    </div>
                    <div className="related__product__price">
                        <span>${props.item.price}</span>
                    </div>
                </div>
            </div>
        </div>
    );
};

RelatedCard.propTypes = {
    item: PropTypes.object
};

export default RelatedCard;
