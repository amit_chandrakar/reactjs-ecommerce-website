import './ItemCard.css';
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { CartItemsContext } from '../../../Context/CartItemsContext';
import { IconButton } from '@mui/material';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import WishItemsContext from '../../../Context/WishItemsContext';
import PropTypes from 'prop-types';

const ItemCard = (props) => {
    const cartItemsContext = useContext(CartItemsContext);
    const wishItemsContext = useContext(WishItemsContext);

    const handleAddToWishList = () => {
        wishItemsContext.addItem(props.item);
    };

    const handleAddToCart = () => {
        cartItemsContext.addItem(props.item, 1);
    };

    // Generate a random number between 1 and 1000
    const random = Math.floor(Math.random() * 50) + 1;

    console.log(props.item);

    return (
        <div className="product__card__card">
            <div className="product__card">
                <div className="product__image">
                    <img src={`https://picsum.photos/id/${random}/500/620`} alt="item" className="product__img"/>
                </div>
                <div className="product__card__detail">
                    <div className="product__name">
                        <Link to={`/product/${props.item.slug}`}>
                            {props.item.name}
                        </Link>
                    </div>
                    <div className="product__description">
                        <span>{props.item.description}</span>
                    </div>
                    <div className="product__price">
                        <span>${props.item.price}</span>
                    </div>
                    <div className="product__card__action">
                        <IconButton onClick={handleAddToWishList} sx={ { borderRadius: '20px', width: '40px', height: '40px' } }>
                            <FavoriteBorderIcon sx={{ width: '22px', height: '22px', color: 'black' }}/>
                        </IconButton>
                        <IconButton onClick={handleAddToCart} sx={ { borderRadius: '20px', width: '40px', height: '40px' }}>
                            <AddShoppingCartIcon sx={{ width: '22px', height: '22px', color: 'black' }}/>
                        </IconButton >
                    </div>
                </div>
            </div>
        </div>
    );
};

ItemCard.propTypes = {
    item: PropTypes.object
};

export default ItemCard;
