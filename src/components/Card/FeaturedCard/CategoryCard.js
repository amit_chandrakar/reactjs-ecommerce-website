import { Link } from 'react-router-dom';
import './CategoryCard.css';
import { Button } from '@mui/material';
import React from 'react';
import PropTypes from 'prop-types';

const CategoryCard = (props) => {
    // Generate a random number between 1 and 1000
    const random = Math.floor(Math.random() * 50) + 1;

    return (
        <div className="category__card__card">
            <div className="category__image">
                <img
                    src={`https://picsum.photos/id/${random}/500/620`}
                    alt="No image found"
                    className="product__img"
                />
            </div>
            <div className="category__card__detail">
                <div className="category__name">
                    <span>{props.data.name}</span>
                </div>
                <div className="category__card__action">
                    <Link to={`category/${props.data.slug}`}>
                        <Button variant='outlined' sx={[{ '&:hover': { backgroundColor: 'none', borderColor: '#FFE26E', color: '#FFE26E' }, borderRadius: '20px', borderColor: '#FFE26E', backgroundColor: '#FFE26E', color: '#000', fontWeight: '700' }]}>SHOP NOW</Button>
                    </Link>
                </div>
            </div>
        </div>
    );
};

CategoryCard.propTypes = {
    data: PropTypes.object
};

export default CategoryCard;
