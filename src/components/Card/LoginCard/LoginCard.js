import { Link } from 'react-router-dom';
import './LoginCard.css';
import React, { useState } from 'react';
import axios from 'axios';

const LoginCard = () => {
    const [formState, setFormState] = useState({
        email: '',
        password: ''
    });

    const handelLogin = async () => {
        if (formState.email === '' || formState.password === '') {
            alert('Please enter email and password');
        } else {
            // Attempt to login
            const response = await axios.post('http://localhost:5001/api/v1/back-end/auth/login', formState);

            localStorage.setItem('accessToken', response.data.data.token);
            localStorage.setItem('loggedInUser', JSON.stringify(response.data.data.user));
            localStorage.setItem('company', JSON.stringify(response.data.data.company));

            setTimeout(() => {
                window.history.pushState(
                    `${
                        process.env.PUBLIC_URL
                            ? process.env.PUBLIC_URL
                            : '/'
                    }`,
                    'login',
                    `${
                        process.env.PUBLIC_URL
                            ? process.env.PUBLIC_URL
                            : '/'
                    }`
                );
                window.location.reload();
            }, 1000);
        }
    };

    const onEmailChange = (event) => {
        setFormState({ ...formState, email: event.target.value });
    };

    const onPasswordChange = (event) => {
        setFormState({ ...formState, password: event.target.value });
    };

    return (
        <div className="login__card__container">
            <div className="login__card">
                <div className="login__header">
                    <h1>Login</h1>
                </div>
                <div className="login__inputs">
                    <div className="email__input__container input__container">
                        <label className="email__label input__label">Email</label>
                        <input
                            type="email"
                            className="email__input login__input"
                            value={formState.email}
                            onChange={onEmailChange}
                        />
                    </div>
                    <div className="password__input__container input__container">
                        <label className="password__label input__label" >Password</label>
                        <input
                            type="password"
                            className="password__input login__input"
                            value={formState.password}
                            onChange={onPasswordChange}
                        />
                    </div>
                    <div className="login__button__container">
                        <button className="login__button" onClick={handelLogin} >LOGIN</button>
                    </div>
                </div>
                <div className="login__other__actions">
                    <div className="login__forgot__password">Forgot password?</div>
                    <div className="login__new__account">Dont have account? <Link to="/account/register">Create account</Link> </div>
                </div>
            </div>
        </div>
    );
};

export default LoginCard;
