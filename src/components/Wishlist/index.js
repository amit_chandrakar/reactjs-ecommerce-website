import React from 'react';
import './index.css';
import { Box, Button, Grid, IconButton } from '@mui/material';
// import '../../components/Card/Cart/Cart.css';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';

const style = {
};

const Wishlist = () => {
    const handelQuantityIncrement = (event) => {
        // cartItems.quantity(props.item.id, 'INC');
    };

    const handelQuantityDecrement = (event) => {
        // if (props.item.itemQuantity > 1) {
        //     cartItems.quantity(props.item.id, 'DEC');
        // }
    };

    const handelRemoveItem = () => {
        // cartItems.removeItem(props.item);
    };

    // Generate a random number between 1 and 1000
    const random = Math.floor(Math.random() * 50) + 1;

    return (
        <>
            <Grid>
                <Box sx={style}>
                    <div className="cart__header mt-4">
                        <h2>Your Cart</h2>
                    </div>
                    <div className="cart__items__container">
                        <div className="cartItems">
                            <div className='cart__item__card'>
                                <div className="cart__item__detail">
                                    <div className="cart__item__image">
                                        <img src={`https://picsum.photos/id/${random}/500/620`} alt="item" className="item__image"/>
                                    </div>
                                    <div className="cart__item__name">AAAA</div>
                                </div>
                                <div className="cart__item__quantity">
                                    <IconButton onClick={handelQuantityDecrement}>
                                        <RemoveCircleIcon fontSize='medium'/>
                                    </IconButton>
                                    <div type="text" name="quantity" className="quantity__input">11</div>
                                    <IconButton onClick={handelQuantityIncrement}>
                                        <AddCircleIcon />
                                    </IconButton>
                                </div>
                                <div className="cart__item__price">$111</div>
                                <div className="remove__item__icon">
                                    <IconButton>
                                        <HighlightOffIcon onClick={handelRemoveItem}/>
                                    </IconButton>
                                </div>
                            </div>
                            <div className='cart__item__card'>
                                <div className="cart__item__detail">
                                    <div className="cart__item__image">
                                        <img src={`https://picsum.photos/id/${random}/500/620`} alt="item" className="item__image"/>
                                    </div>
                                    <div className="cart__item__name">AAAA</div>
                                </div>
                                <div className="cart__item__quantity">
                                    <IconButton onClick={handelQuantityDecrement}>
                                        <RemoveCircleIcon fontSize='medium'/>
                                    </IconButton>
                                    <div type="text" name="quantity" className="quantity__input">11</div>
                                    <IconButton onClick={handelQuantityIncrement}>
                                        <AddCircleIcon />
                                    </IconButton>
                                </div>
                                <div className="cart__item__price">$111</div>
                                <div className="remove__item__icon">
                                    <IconButton>
                                        <HighlightOffIcon onClick={handelRemoveItem}/>
                                    </IconButton>
                                </div>
                            </div>
                            <div className='cart__item__card'>
                                <div className="cart__item__detail">
                                    <div className="cart__item__image">
                                        <img src={`https://picsum.photos/id/${random}/500/620`} alt="item" className="item__image"/>
                                    </div>
                                    <div className="cart__item__name">AAAA</div>
                                </div>
                                <div className="cart__item__quantity">
                                    <IconButton onClick={handelQuantityDecrement}>
                                        <RemoveCircleIcon fontSize='medium'/>
                                    </IconButton>
                                    <div type="text" name="quantity" className="quantity__input">11</div>
                                    <IconButton onClick={handelQuantityIncrement}>
                                        <AddCircleIcon />
                                    </IconButton>
                                </div>
                                <div className="cart__item__price">$111</div>
                                <div className="remove__item__icon">
                                    <IconButton>
                                        <HighlightOffIcon onClick={handelRemoveItem}/>
                                    </IconButton>
                                </div>
                            </div>

                            <div className="options">
                                <div className="total__amount">
                                    <div className="total__amount__label">Total Amount:</div>
                                    <div className="total__amount__value">$11.00</div>
                                </div>
                                <div className="checkout">
                                    <Button variant="outlined">Checkout</Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </Box>
            </Grid>
        </>
    );
};

export default Wishlist;
