import { Link } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import './NavLinks.css';
import axios from 'axios';

const NavLinks = () => {
    const [categories, setCategories] = useState([]);

    // Function to fetch all the categories
    const fetchCategories = async () => {
        try {
            const response = await axios.get('http://localhost:5001/api/v1/back-end/categories');
            setCategories(response.data.categories);
        } catch (error) {
            console.error(error);
        }
    };

    // Function to get all the categories on page load
    useEffect(() => {
        fetchCategories();
    }, []);

    return (
        <nav className="nav__bottom__container">
            <div className="bottom__container">
                <ul className="nav">
                    <li className='nav-link'><Link to="/">Home</Link></li>
                    {categories.map((category) => (
                        <li key={category._id} className='nav-link'>
                            <Link to={`/category/${category.slug}`}>{category.name}</Link>
                        </li>
                    ))}
                </ul>
            </div>
        </nav>
    );
};

export default NavLinks;
