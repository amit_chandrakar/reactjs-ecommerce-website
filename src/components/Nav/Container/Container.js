import React, { useEffect } from 'react';
import Control from '../Controls/Control';
import DrawerNav from '../DrawerNav/DrawerNav';
import NavBrand from '../Nav-Brand/Navbrand';
import Form from '../Search-Bar/Form';
import './Container.css';

const Navtop = () => {
    useEffect(() => {
        // Check if local storage has a sessionId
        const sessionId = localStorage.getItem('sessionId');

        if (!sessionId) {
            // Create a new sessionId
            const newSessionId = Math.floor(Math.random() * 100000000000000000);
            localStorage.setItem('sessionId', newSessionId);
        }
    }, []);

    return (
        <div className="nav__top__container">
            <div className="top__container">
                <NavBrand />
                <div className="form__container">
                    <Form />
                </div>
                <div className="control__bar">
                    <Control />
                </div>
                <div className="drawer">
                    <DrawerNav />
                </div>
            </div>
        </div>
    );
};

export default Navtop;
