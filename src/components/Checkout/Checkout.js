import React from 'react';
import { Box, Grid, TextField } from '@mui/material';
import CardContent from '@mui/material/CardContent';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary
}));

const Checkout = () => {
    return (
        <>
            <Grid>
                <Box>
                    <div className="cart__header mt-4">
                        <h2>Checkout</h2>
                    </div>
                    <CardContent>

                        <Grid container spacing={5}>
                            <Grid item xs={8}>
                                <Grid container spacing={4}>
                                    <Grid item xs={4}>
                                        <TextField fullWidth label="Name" id="fullWidth" />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <TextField fullWidth label="Name" id="fullWidth" />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <TextField fullWidth label="Name" id="fullWidth" />
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={4}>
                                <Item>xs=4</Item>
                                <Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </CardContent>

                </Box>
            </Grid>
        </>
    );
};

export default Checkout;
