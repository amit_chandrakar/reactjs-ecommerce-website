import React, { useEffect, useState } from 'react';
import CategoryCard from '../../Card/FeaturedCard/CategoryCard';
import './FeaturedCategories.css';
import axios from 'axios';

const Categories = () => {
    const [categories, setCategories] = useState([]);

    // Function to fetch all the categories
    const fetchCategories = async () => {
        try {
            const response = await axios.get('http://localhost:5001/api/v1/back-end/categories');
            setCategories(response.data.categories);
        } catch (error) {
            console.error(error);
        }
    };

    // Function to get all the categories on page load
    useEffect(() => {
        fetchCategories();
    }, []);

    return (
        <div className="featured__categories__container">
            <div className="featured__categories">
                <div className="featured__categories__header">
                    <h1 className='featured__header__big'>Featured Categories </h1>
                    <div className="featured__categories__header__line"></div>
                </div>
                <div className="featured__categories__card__container">
                    { categories.map((category) => <CategoryCard key={category._id} data={category}/>)}
                </div>
            </div>
        </div>
    );
};

export default Categories;
