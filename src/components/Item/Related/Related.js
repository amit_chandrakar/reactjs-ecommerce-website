import React, { useState, useEffect } from 'react';
import axios from 'axios';
import RelatedCard from '../../Card/RelatedCard/RelatedCard';
import './Related.css';
import PropTypes from 'prop-types';

const Related = (props) => {
    const [items, setItems] = useState([]);

    // Get all the products by category slug
    const fetchProductsByCategorySlug = async () => {
        try {
            const response = await axios.get('http://localhost:5001/api/v1/front-end/get-category-by-slug', { params: { slug: props.category.slug } });
            setItems(response.data.products);
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        fetchProductsByCategorySlug();
    }, []);

    return (
        <div className="related__products">
            <div className="related__header__container">
                <div className="related__header">
                    <h2>Recommended Products</h2>
                </div>
                <div className="related__header__line">

                </div>
            </div>
            <div className="related__card__container">
                <div className="related__product__card">
                    { items && items.map((item) => <RelatedCard key="1" item={item}/>)}
                </div>
            </div>
        </div>
    );
};

Related.propTypes = {
    category: PropTypes.object
};

export default Related;
