import './Description.css';
import React from 'react';
import PropTypes from 'prop-types';

const Description = (props) => {
    return (
        <div className="product__description__product">
            <div className="description__header__container">
                <div className="description__header__line"></div>
                <div className="description__header">Details</div>
            </div>
            <div className="description__specifics__container">
                <div className="description__specifics">
                    <div className="description__header__line"></div>
                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                </div>
            </div>
        </div>
    );
};

Description.propTypes = {
    item: PropTypes.object
};

export default Description;
