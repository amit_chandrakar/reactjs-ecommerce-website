import ItemCarousel from './Carousel/ItemCarousel';
import Detail from './Detail/Detail';
import Description from './Description/Description';
import Related from './Related/Related';
import './Item.css';
import React from 'react';
import PropTypes from 'prop-types';

const Item = (props) => {
    return (
        <div className="item__container">
            <div className="detail__and__carousel__container">
                <ItemCarousel item={props.item}/>
                <Detail item={props.item}/>
            </div>
            <div className="item__description__container">
                <Description item={props.item}/>
            </div>
            <div className="related__items__container">
                <Related category={props.item.category}/>
            </div>
        </div>
    );
};

Item.propTypes = {
    item: PropTypes.object
};

export default Item;
