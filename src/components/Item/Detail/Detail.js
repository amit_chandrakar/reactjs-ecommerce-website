import React, { useContext, useState } from 'react';
import './Detail.css';
import { Button, IconButton } from '@mui/material';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import { CartItemsContext } from '../../../Context/CartItemsContext';
import PropTypes from 'prop-types';

const Detail = (props) => {
    const [quantity, setQuantity] = useState(1);

    const cartItems = useContext(CartItemsContext);

    const handelQuantityIncrement = (event) => {
        setQuantity((prev) => prev + 1);
    };

    const handelQuantityDecrement = (event) => {
        if (quantity > 1) {
            setQuantity((prev) => prev - 1);
        }
    };

    const handelAddToCart = () => {
        cartItems.addItem(props.item, quantity);
        alert('Item added to cart');
    };

    return (
        <div className="product__detail__container">
            <div className="product__detail">
                <div className="product__main__detail">
                    <div className="product__name__main">{props.item.name}</div>
                    <div>{props.item.category.name}</div>
                    <div className="product__detail__description">{props.item.description}</div>
                    <div className="product__price__detail">${props.item.price}</div>
                </div>
                <form onSubmit={handelAddToCart} className="product__form">
                    <div className="product__quantity__and__size">
                        <div className="product__quantity">
                            <IconButton onClick={handelQuantityDecrement}>
                                <RemoveCircleIcon fontSize='medium'/>
                            </IconButton>
                            <div type="text" name="quantity" className="quantity__input">{quantity}</div>
                            <IconButton onClick={handelQuantityIncrement}>
                                <AddCircleIcon />
                            </IconButton>
                        </div>
                    </div>
                    <div className="collect__item__actions">
                        <div className="add__cart__add__wish">
                            <div className="add__cart">
                                <Button variant="outlined" size="large" sx={[{ '&:hover': { backgroundColor: '#FFE26E', borderColor: '#FFE26E', borderWidth: '3px', color: 'black' }, minWidth: 200, borderColor: 'black', backgroundColor: 'black', color: '#FFE26E', borderWidth: '3px' }]} onClick={handelAddToCart}>ADD TO BAG</Button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    );
};

Detail.propTypes = {
    item: PropTypes.object
};

export default Detail;
