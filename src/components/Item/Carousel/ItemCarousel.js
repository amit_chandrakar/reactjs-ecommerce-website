import Carousel from 'react-bootstrap/Carousel';
import './ItemCarousel.css';
import React from 'react';
import PropTypes from 'prop-types';

const ProductCarousel = (props) => {
    return (
        <div className="product__carousel__container">
            <div className="product__carousel">
                <Carousel variant="dark" interval={4000}>
                    <Carousel.Item>
                        <div className="carousel__image__container">
                            <img className="carousel__image" src="https://fastly.picsum.photos/id/28/500/620.jpg?hmac=XaaqkS6uCi_yNSiLDBk8HkaUOdi17YAdTaR72GtSAnA" alt="item"/>
                        </div>
                    </Carousel.Item>
                    <Carousel.Item>
                        <div className="carousel__image__container">
                            <img className="carousel__image" src="https://fastly.picsum.photos/id/25/500/620.jpg?hmac=D4gV78Xj7fGTisH_IPSypdl4PIswgAMWXTH6H4RM35A" alt="item"/>
                        </div>
                    </Carousel.Item>
                </Carousel>
            </div>
        </div>
    );
};

ProductCarousel.propTypes = {
    item: PropTypes.object
};

export default ProductCarousel;
