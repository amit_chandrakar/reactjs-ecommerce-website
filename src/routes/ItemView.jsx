import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import ReactLoading from 'react-loading';
import Item from '../components/Item/Item';

const ProductView = (props) => {
    const param = useParams();
    const [item, setItem] = useState();
    const [loading, setLoading] = useState(true);

    // Get all the products by category slug
    const fetchProductsBySlug = async () => {
        try {
            const response = await axios.get('http://localhost:5001/api/v1/front-end/get-product-by-slug', { params: { slug: param.id } });
            setItem(response.data);
        } catch (error) {
            console.error(error);
        }

        setLoading(false);
    };

    useEffect(() => {
        window.scrollTo(0, 0);
        fetchProductsBySlug();
    }, [param.id]);

    return (
        <div className="d-flex min-vh-100 w-100 justify-content-center align-items-center m-auto">
            {loading && <ReactLoading type="balls" color='#FFE26E' height={100} width={100} className='m-auto'/>}
            {item && <Item item={item}/>}
        </div>
    );
};

export default ProductView;
