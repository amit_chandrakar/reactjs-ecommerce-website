import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import ReactLoading from 'react-loading';
import Category from '../components/Category/Category';

const CategoryView = () => {
    const param = useParams();
    const [loading, setLoading] = useState(true);
    const [items, setItems] = useState([]);
    const [category, setCategory] = useState('');

    // Get all the products by category slug
    const fetchProductsByCategorySlug = async () => {
        try {
            const response = await axios.get('http://localhost:5001/api/v1/front-end/get-category-by-slug', { params: { slug: param.id } });
            setItems(response.data.products);
            setCategory(response.data.name);
        } catch (error) {
            console.error(error);
        }

        setLoading(false);
    };

    useEffect(() => {
        fetchProductsByCategorySlug();
        window.scrollTo(0, 0);
    }, [param.id]);

    console.log(items);

    return (
        <div className='d-flex min-vh-100 w-100 justify-content-center align-items-center m-auto'>
            {loading && <ReactLoading type="balls" color='#FFE26E' height={100} width={100} className='m-auto'/>}
            { items && <Category name={category} items={items} category={category} />}
        </div>
    );
};

export default CategoryView;
