import React, { Fragment, useEffect, useState } from 'react';
import axios from 'axios';
import Landing from '../components/Landing/Landing';
import FeaturedItems from '../components/Featured/Items/FetauredItems';
import FeaturedCategories from '../components/Featured/Categories/FeaturedCategories';
import { TabTitle } from '../utils/General';

const Home = () => {
    const [featuredItems, setFeaturedItems] = useState();
    TabTitle('Home - Ecomelite');

    useEffect(() => {
        axios.get('http://localhost:5001/api/v1/front-end/get-featured-products')
            .then(res => setFeaturedItems(res.data))
            .catch(err => console.log(err));

        window.scrollTo(0, 0);
    }, []);

    return (
        <>
            <Landing />
            <FeaturedCategories />
            <FeaturedItems items={featuredItems}/>
        </>
    );
};

export default Home;
