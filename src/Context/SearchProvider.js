import React, { useState } from 'react';
import SearchContext from './SearchContext';
import PropTypes from 'prop-types';

const SearchProvider = (props) => {
    const [query, setQuery] = useState();

    const setSearchQuery = (queryParam) => {
        setQuery(queryParam);
    };

    const searchCtx = {
        searchQuery: query,
        setSearchQuery
    };
    return (
        <SearchContext.Provider value={searchCtx}>
            {props.children}
        </SearchContext.Provider>
    );
};

SearchProvider.propTypes = {
    children: PropTypes.node
};

export default SearchProvider;
