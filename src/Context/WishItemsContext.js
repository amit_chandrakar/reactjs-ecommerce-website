import { createContext } from 'react';

const WishItemsContext = createContext({
    items: [],
    addItem: () => {},
    removeItem: () => {},
    addToCart: () => {}
});

export default WishItemsContext;
