const TabTitle = (newTitle) => {
    return (document.title = newTitle);
};

const APP_URL = process.env.REACT_APP_ENV === 'live' ? process.env.REACT_APP_API_BASE_URL_LIVE : process.env.REACT_APP_API_BASE_URL_LOCAL;

export { TabTitle, APP_URL };
